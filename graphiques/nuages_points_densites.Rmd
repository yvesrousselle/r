---
title: "Nuages de points et densités"
author: "yves.rousselle@onf.fr"
date: "`r format(Sys.time(), '%d %b %Y')`"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
# nettoyage de l'espace de travail
rm(list = ls())
```

# Objet de ce script

Ce script a pour objet de présenter des fonctions simples et présentes
dans R de base pour représenter graphiquement des nuages de points
à 2 variables et la densité de ces nuages.

# Les données

Je crée 2 vecteurs :

* x selon 2 lois normales et avec une bimodalité
* y selon une loi normale

```{r}
x <- x <- c(rnorm(5000), rnorm(5000, 3))
y <- rnorm(10000)
```

```{r}
hist(x)
hist(y)
plot(x, y)
```

# Avec `smoothScatter()`

Je cite l'aide :

*`smoothScatter` produces a smoothed color density representation of*
*a scatterplot, obtained through a (2D) kernel density estimate.*

L'estimation de la densité locale est réalisée grâce à la fonction
`bkde2D` du paquet `KernSmooth`.

```{r}
smoothScatter(x, y)
```

# Avec `densCols()` dans `plot()`

L'aide :

*`densCols` produces a vector containing colors which encode the local*
*densities at each point in a scatterplot.*

La densité locale est estimé avec la même fonction que pour 
`smoothScatter`.

```{r}
plot(x, y, col = densCols(x, y))
plot(x, y, col = densCols(x, y), pch = 3)
plot(x, y, col = densCols(x, y), pch = 4)
plot(x, y, col = densCols(x, y), pch = 16)
plot(x, y, col = densCols(x, y), pch = 20)
```

# De la couleur ?

La couleur est géré par l'argument `colramp`. Cet argument attend une
fonction qui peut prendre comme argument un entier *n* et qui renvoit
*n* couleurs.

Dit comme ça, c'est peut-être pas hyper clair mais la valeur par
défaut permet de mieux comprendre : 
`colorRampPalette(blues9[-(1:3)]))`.

L'aide de `colorRampPalette` fait référence à `topo.colors` et donc,
après quelques essais, il suffit de mettre le nom de la fonction 
souhaitée :

```{r}
plot(x, y,
     col = densCols(x, y, colramp = heat.colors),
     pch = 16)
plot(x, y,
     col = densCols(x, y, colramp = rainbow),
     pch = 16)
plot(x, y,
     col = densCols(x, y, colramp = terrain.colors),
     pch = 16)
plot(x, y,
     col = densCols(x, y, colramp = topo.colors),
     pch = 16)
plot(x, y,
     col = densCols(x, y, colramp = cm.colors),
     pch = 16)
```

